//Crear una clase Doctor, que contenga un nombre, apellido, y código de doctor, debe también contener un método llamado diagnosticar, que debería logear en consola el siguiente string: “Estoy diagnosticando al paciente”. 
//===========================================================================================

//===========================================================================================
//Luego, crearemos una clase llamada Neurólogo, la cual deberá extender de Doctor. Esta nueva clase va a contener otro nuevo método llamado tomar radiografía, debe ser privado, y deberá logear en consola lo siguiente: “Tomando Radiografía...”.
//===========================================================================================

//===========================================================================================
//Además de esto, este método privado debería ser llamado cuando se llame el método de la clase padre Doctor ‘diagnosticar’, conservando la misma funcionalidad de su padre, solo agregando la llamada a este nuevo método.
//===========================================================================================

//===========================================================================================
//Por último, crearemos una clase llamada Pediatra, la misma extiende también de la clase Doctor, está, cuando se llame la función de su padre llamada ‘diagnosticar’ deberá logear en consola lo siguiente: “Estoy diagnosticando al niño”, sobreescribiendo así, la funcionalidad de su padre por completo.
//===========================================================================================

//   *****  NOTAS  *****
//Orden:
// Primero Propiedades
// Segundo Constructor
// Tercero Metodos
//Sub-orden
// Privados
// Protegidos
// Publicos
//
//   *****  Privates  *****
// Llevan _ antes del nombre.

//===========================================================================================
//===========================================================================================

function rand(min: number, max: number) {
    let randomNum = Math.random() * (max - min) + min;
    return Math.floor(randomNum);
}

class Doctor {
    public nombre: string;
    public apellido: string;
    public codigo: number;

    constructor(nombre: string, apellido: string, codigo: number) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.codigo = codigo;
    }

    public diagnosticar() { // Funciones dentro de clases no llevan palabra function
        return "Estoy diagnosticando al paciente";
    }
}

class Neurologo extends Doctor {
    private _tomarRadiografia() {
        console.log("Tomando Radiografía...");
    }

    public diagnosticar() {
        this._tomarRadiografia();
        return "Estoy diagnosticando al paciente";
    }
}

class Pediatra extends Doctor {
    public diagnosticar() {
        return "Estoy diagnosticando al niño";
    }
}

const cantidadDeDoctores: number = 5;
const listaDeDoctores: Doctor[] = [];

while (listaDeDoctores.length < cantidadDeDoctores) {
    const radar = rand(0, 150);
    let doctor: Doctor = null;
    if (radar >= 0 && radar <= 50) {
        doctor = new Doctor("Diego", "Maradona", radar);
    } else if (radar <= 100) {
        doctor = new Neurologo("Katie", "Perry", radar);
    } else {
        doctor = new Pediatra("Karen", "Kaka", radar);
    }

    listaDeDoctores.push(doctor);
}

for (const doctor of listaDeDoctores) {
    console.log(doctor.nombre + " " + doctor.apellido + " " + doctor.codigo);
    console.log(doctor.diagnosticar());
    console.log(" ");
}